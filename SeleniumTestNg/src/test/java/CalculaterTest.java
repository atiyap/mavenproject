import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CalculaterTest {

    Calculater calculater;
    @BeforeTest
    public void createObject(){

         calculater = new Calculater();
    }

    @Test
    public void addTest(){

        Assert.assertEquals(30,calculater.add(10,20));

    }

    @Test
    public void subtractTest(){

        Assert.assertEquals(10,calculater.subtract(20,10));
    }

    @Test
    public void multiplyTest(){

        Assert.assertEquals(100,calculater.multiply(10,10));

    }

}
