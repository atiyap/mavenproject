import org.testng.annotations.*;

public class ZoomTest {

    @BeforeSuite
    public void setUpAtSuiteLevel(){
        System.out.println("Running at suite level");
    }

    @BeforeClass
    public void setUpAtClassLevel(){
        System.out.println("Running before Class");
    }

    @BeforeMethod
    public void beforeMethod(){
        System.out.println("Running before method");
    }


    @BeforeTest
    public void beforeTest(){
        System.out.println("Running before test");
    }

    @Test
    public void simpletest(){
        System.out.println("Simple Test");
    }

    @AfterSuite
    public void removeSuiteLevel(){
        System.out.println("Removing at suite level");
    }

    @AfterClass
    public void removeClassLevel(){
        System.out.println("Remove Class");
    }

    @AfterMethod
    public void afterMethod(){
        System.out.println("Running after method");
    }


    @AfterTest
    public void afterTest(){
        System.out.println("Running after test");
    }
}
